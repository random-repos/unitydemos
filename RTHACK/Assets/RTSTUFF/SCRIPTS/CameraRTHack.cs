﻿using UnityEngine;
using System.Collections;

//add me to the camera you want to render as a render texture
public class CameraRTHack : MonoBehaviour
{
	RenderTexture rt;
	void Start () {
		//third argument is bit depth of the RenderTexture's depth buffer. 16 is fine.
		//note RenderTexture derives from Texture so you can use it whenever you can use a Texture
		//note you'll need to recreate/resize your RenderTexture if you resize the screen
		rt = new RenderTexture(Screen.width,Screen.height,16);
		camera.targetTexture = rt;
	}
	/*
	//this code will allow you to control camera rendering through code instead
	//probably not useful, but this is how I usually do it because I prefer controlling things through code. 
	//note I think the camera only renders properly if it's enabled, and you'll want to keep it disabled in general so it does not also draw to the screen
	public static void draw_render_texture(Camera aCam, RenderTexture aRT)
	{
		bool prevState = aCam.enabled;
		aCam.enabled = true;
		RenderTexture.active = aRT;
		RenderTexture prevTarTex = aCam.targetTexture;
		aCam.targetTexture = aRT;
		aCam.backgroundColor = new Color(1,1,1,0);
		aCam.clearFlags = CameraClearFlags.SolidColor;
		aCam.DoClear();
		aCam.Render();
		aCam.targetTexture = prevTarTex;
		RenderTexture.active = null;
		aCam.enabled = prevState;
	}

	// Update is called once per frame
	void Update () {
		camera.enabled = false;
		draw_render_texture(camera,rt);
	}*/

	void OnGUI()
	{
		//the smaller depth gets drawn on top
		GUI.depth = int.MinValue;
		GUI.Label(new Rect(0,0,Screen.width,Screen.height),rt);
	}
}
