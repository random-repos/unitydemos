﻿using UnityEngine;
using System.Collections;

public class collisionDetection : MonoBehaviour {
	//public void OnCollisionEnter()
	public void OnTriggerEnter(Collider col)
	{
		//note here the scoreTracker component must be on the main camera or this wont work
		Camera.main.GetComponent<scoreTracker>().score += 1;

		col.transform.position = Vector3.zero;
	}
}
