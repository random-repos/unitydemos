﻿using UnityEngine;
using System.Collections;

public class keyboradControl : MonoBehaviour {

	public float speed = 1;
	
	void Start () {
		//nothingh to do here :)
	}
	
	void Update () {
		
		Vector3 change = Vector3.zero;
		
		//read the user input
		if(Input.GetKey(KeyCode.UpArrow))
		{
			change.y = 1;
		}
		if(Input.GetKey(KeyCode.DownArrow))
		{
			change.y = -1;
		}
		if(Input.GetKey(KeyCode.LeftArrow))
		{
			change.x = -1;
		}
		if(Input.GetKey(KeyCode.RightArrow))
		{
			change.x = 1;
		}
		
		//use the speed we set in the editor
		change = change * speed;
		
		//actually set the velocity of the rigidbody this script is attached too
		rigidbody.velocity = change;
	}
}
