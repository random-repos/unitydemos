﻿using UnityEngine;
using System.Collections;

public class mouseControl : MonoBehaviour {

	Vector3 basePosition;
	public float strength = 1;
	void Start()
	{
		//store the original position of the gameobject
		basePosition = transform.position;
	}
	void Update()
	{
		//remember, screen coordinates are different from world coordinates so we need to convert them.
		//this conversion is dependent on which camera is looking into the world
		//luckily unity has a function that takes care of all this for you
		//Camera.main is a reference to the main camera. If you only have one camera in the scene, its that one.
		//note that Unity can't know how "deep" you clicked in the screen so we need to set this ourselves
		//by the way, here we assume the camera is looking down the z axis, if it's not, this code won't work right.
		Vector3 screenPosition = Input.mousePosition; //by default, this sets the z to 0, which is the plane the camera is on
		screenPosition.z = (transform.position-Camera.main.transform.position).z;
		Vector3 worldPoint = Camera.main.ScreenToWorldPoint(screenPosition);


		//in this case, we apply a force to the rigidbody in the direction of where our mouse is
		//this will create a smoother motion
		//note fun things will happen if you make strength too large ;)
		rigidbody.AddForce((worldPoint-transform.position)*strength,ForceMode.Force);
	}
}
